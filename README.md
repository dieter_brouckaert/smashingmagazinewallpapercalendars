# README #

This application downloads the latest wallpaper calendars from [smashing magazine](www.smashingmagazine.com/tag/wallpapers/).

### How to set up? ###

* Build the application, or get the latest release from the bin/debug folder.
* Edit the configuration file to your needs. Configuration file is explained below.

#### The configuration file ####
*  WallpaperFolder -> Folder where all the wallpapers will be placed.
* Resolution1 ->The first resolution that will be downloaded.
* Resolution2 ->If the first resolution isn't available, the second resolution will be downloaded.
* Resolution3 ->If the second resolution isn't available, the third resolution will be downloaded.
* WithCalendar -> Should the calendar be displayed on the wallpaper?
* OnlyLatestWallpapers -> Download only the latest wallpapers.
* AskToClose -> When everything is finished the application will stay open

##### Possible resolutions #####
* 2560x1440
* 1920x1440
* 1920x1200
* 1920x1080
* 1680x1200
* 1680x1050
* 1600x1200
* 1440x900
* 1400x1050
* 1280x1024
* 1280x960
* 1280x800
* 1280x720
* 1152x864
* 1024x1024
* 1024x768
* 800x600
* 800x480
* 640x480
* 320x480