﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using WallpaperCalendars.App.Properties;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using WallpaperCalendars.App.Helper;

namespace WallpaperCalendars.App
{
    public class Program
    {
        static void Main(string[] args)
        {
            try { RunProgram(); } catch { }
        }

        private static void RunProgram()
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(Settings.Default.WallpaperFolder);

            HtmlWeb hw = new HtmlWeb();
            HtmlDocument doc = new HtmlDocument();

            string html;
            using (var wc = new GzipWebClient())
                html = wc.DownloadString("http://www.smashingmagazine.com/tag/wallpapers/");

            doc.LoadHtml(html);

            HtmlNodeCollection linkNodes = doc.DocumentNode.SelectNodes("//a[@href]");

            HashSet<string> urls = new HashSet<string>();

            foreach (HtmlNode linkNode in linkNodes)
                if (linkNode.Attributes["href"] != null)
                    if (linkNode.Attributes["href"].Value.Contains("desktop-wallpaper-calendars-")
                        && (linkNode.Attributes["href"].Value.EndsWith(DateTime.Now.AddYears(-1).Year.ToString() + "/")
                        || linkNode.Attributes["href"].Value.EndsWith(DateTime.Now.Year.ToString()+"/")
                        || linkNode.Attributes["href"].Value.EndsWith(DateTime.Now.AddYears(1).Year.ToString() + "/")))
                    {
                        string url = linkNode.Attributes["href"].Value;

                        if (!url.Contains("http"))
                            url = "http://" + url;

                        urls.Add(url);
                        Console.WriteLine("Url with wallpapers -> " + url);

                        if (Settings.Default.OnlyLatestWallpapers && urls.Any())
                            break;
                    }

            for (int i = urls.Count-1; i >= 0; i--)
            {
                DirectoryInfo subDirectory = new DirectoryInfo(Settings.Default.WallpaperFolder);

                string urlToLoad = urls.ElementAt(i);
                string title = string.Empty;

                using(var wc = new GzipWebClient())
                    html = wc.DownloadString(urlToLoad);

                //doc = hw.Load(urlToLoad);
                doc.LoadHtml(html);
                Console.WriteLine("Loaded url -> " + urlToLoad);

                var h2Nodes = doc.DocumentNode.SelectNodes("//h2");
                if (h2Nodes.Any())
                {
                    title = h2Nodes.FirstOrDefault(x => x.InnerText.Contains("Desktop Wallpaper Calendars") && x.InnerText.Contains(":")).InnerText.Split(':')[1].Trim();
                    Console.WriteLine("Wallpapers -> " + title);
                }

                if (string.IsNullOrWhiteSpace(title))
                    continue;

                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                    Console.WriteLine("Created wallpaper directory");
                }

                if (!Settings.Default.OnlyLatestWallpapers)
                {
                    if (directoryInfo.GetDirectories(title).Any())
                    {
                        Console.WriteLine("Wallpaper directory " + title + " already exists");
                        continue;
                    }

                    subDirectory = directoryInfo.CreateSubdirectory(title);
                    Console.WriteLine("Created wallpaper directory " + title);
                }

                foreach (System.IO.FileInfo file in directoryInfo.GetFiles())
                    file.Delete();
                Console.WriteLine("Deleted wallpapers in main folder ");

                var h3Nodes = doc.DocumentNode.SelectNodes("//h3");
                var anchorNodes = doc.DocumentNode.SelectNodes("//a");
                foreach (var h3 in h3Nodes)
                {
                    var wallpaperTitle = Regex.Replace(WebUtility.HtmlDecode(h3.InnerText), @"[^A-Za-z0-9_ ]", "");

                    string[] imageUrls = new string[0];
                    if (Settings.Default.WithCalendar)
                    {
                        imageUrls = anchorNodes.Where(x => x.Attributes["title"] != null && Regex.Match(Regex.Replace(x.Attributes["title"].Value, @"[^A-Za-z0-9_ ]", ""), wallpaperTitle, RegexOptions.IgnoreCase).Success && x.Attributes["href"] != null && x.Attributes["href"].Value.Contains("cal") && !x.Attributes["href"].Value.Contains("nocal")).Select(y => y.Attributes["href"].Value).ToArray();
                    }
                    else
                    {
                        imageUrls = anchorNodes.Where(x => x.Attributes["title"] != null && Regex.Match(Regex.Replace(x.Attributes["title"].Value, @"[^A-Za-z0-9_ ]", ""), wallpaperTitle, RegexOptions.IgnoreCase).Success && x.Attributes["href"] != null && x.Attributes["href"].Value.Contains("nocal")).Select(y => y.Attributes["href"].Value).ToArray();
                    }

                    var imageUrlResolution1 = imageUrls.FirstOrDefault(x => x.Contains(Settings.Default.Resolution1));
                    if (imageUrlResolution1 != null)
                    {
                        DownloadAndSaveImage(directoryInfo, subDirectory, imageUrlResolution1);
                        continue;
                    }

                    var imageUrlResolution2 = imageUrls.FirstOrDefault(x => x.Contains(Settings.Default.Resolution2));
                    if (imageUrlResolution2 != null)
                    {
                        DownloadAndSaveImage(directoryInfo, subDirectory, imageUrlResolution2);
                        continue;
                    }

                    var imageUrlResolution3 = imageUrls.FirstOrDefault(x => x.Contains(Settings.Default.Resolution3));
                    if (imageUrlResolution3 != null)
                    {
                        DownloadAndSaveImage(directoryInfo, subDirectory, imageUrlResolution3);
                        continue;
                    }
                }
            }

            Console.WriteLine("Press any key to close ...");
            if (Settings.Default.AskToClose)
                Console.ReadKey();
        }

        private static void DownloadAndSaveImage(DirectoryInfo mainDirectory, DirectoryInfo subDirectory, string imageUrlResolution)
        {
            WebClient wc = new WebClient();

            string fileName = System.IO.Path.GetFileName(imageUrlResolution);

            if (!Settings.Default.OnlyLatestWallpapers)
                wc.DownloadFile(imageUrlResolution, Path.Combine(subDirectory.FullName, fileName));

            wc.DownloadFile(imageUrlResolution, Path.Combine(mainDirectory.FullName, fileName));

            Console.WriteLine("Downloaded -> " + fileName);
        }
    }
}
